import shared

import mediawiki_api_wrapper
import mwparserfromhell

import datetime
import time
import webbrowser
import re

def skip_count():
    return 3000

def selftest():
    if is_marked_already("{{Proper attribution missing|template=CC-BY-4.0|subcategory=uploader notified 2022, May}}") != True:
        raise
    fixed = fix_template_capitalization("{{CC-by-sa-2.0}}")
    if fixed != "{{CC-BY-SA-2.0}}":
        print(fixed)
        raise

def main():
    selftest()
    apply_requests_and_detect_invalid_ones()

def apply_requests_and_detect_invalid_ones():
    session = shared.create_login_session('image_bot')
    watchlisting_session = shared.create_login_session()

    # collect pages already with requests already listed - this avoids querying their page text
    report_present_already = []
    for page_title in mediawiki_api_wrapper.query.pages_from_category("Category:Media without a proper attribution"):
        report_present_already.append(page_title)
    
    report_was_required = []

    banned_users = shared.users_dropped_from_regular_processing()
    categories = [
        "Category:Attribution not provided in CC license template which requires attribution",
    ]
    skip = skip_count()
    processed = 0
    for category in categories:
        for page_title in mediawiki_api_wrapper.query.pages_from_category(category):
            processed += 1
            if processed < skip:
                print(processed, "skipped")
                continue
            print(processed, "processed")
            report_was_required.append(page_title)
            if page_title in report_present_already:
                continue
            uploader = shared.get_uploader_of_file_or_none_if_not_clear(page_title)
            if uploader == None:
                continue
            if uploader in banned_users:
                continue
            banned_users.append(uploader) # will be now processed either way

            data = mediawiki_api_wrapper.query.download_page_text_with_revision_data(page_title)
            text = data['page_text']
            if is_marked_already(text):
                # extra download per user, will protect against processing hundreds of images
                # where for all relevant user was notified anyway 
                continue
            if skip_file(text, page_title):
                continue
            try:
                files = get_all_affected_uploads_by_user(uploader)
            except mediawiki_api_wrapper.query.InvalidUsername as e:
                print("page_title:", page_title, "uploader:", uploader)
            if files == []:
                print(uploader, "is apparently fully notified, also for", page_title, "which triggered checks")
                continue # notified already

            print("===========================================")
            print("============START<=========================")
            print("===========================================")
            list_of_likely_editable_ones = []
            list_where_asking_is_still_a_good_idea = []
            for file_data in files:
                likely_editable = False
                file = file_data['filename']
                file_page = mediawiki_api_wrapper.query.download_page_text_with_revision_data(file)
                text = file_page['page_text']
                print()
                print()
                print("======================================")
                print()
                print()
                print(mediawiki_api_wrapper.interface.osm_wiki_page_link(file))
                print(mediawiki_api_wrapper.interface.osm_wiki_page_edit_link(file))
                print(file_page['page_text'])
                print()
                for keyword in shared.description_keywords_that_may_indicate_that_author_is_specified():
                    if keyword.lower() in text.lower():
                        likely_editable = True
                if likely_editable:
                    list_of_likely_editable_ones.append(file_data)
                else:
                    list_where_asking_is_still_a_good_idea.append(file_data)

            if len(list_of_likely_editable_ones) > 0:
                print(uploader, "apparently has files which are marked, just not in the template")
                print("skipping them")
                for file_data in list_of_likely_editable_ones:
                    print(mediawiki_api_wrapper.interface.osm_wiki_page_link(file_data['filename']))
                print(len(list_where_asking_is_still_a_good_idea), "where asking may be still a good idea")
                for file_data in list_where_asking_is_still_a_good_idea:
                    print(mediawiki_api_wrapper.interface.osm_wiki_page_link(file_data['filename']))
                print("===========================================")
                print("============>END===========================")
                print("===========================================")
                print()
                continue # notified already

            print(uploader, "-", "notify them about problems?")
            outcome = shared.ask_human()
            if outcome:
                session = notify_user_on_their_talk_page(session, uploader, list_where_asking_is_still_a_good_idea)
                for file_data in list_where_asking_is_still_a_good_idea:
                    session, watchlisting_session = mark_file_as_missing_clear_attribution(session, watchlisting_session, file_data)
                create_category_for_the_current_month_if_missing(session)

    print("Following entries were found to carry requests to fill attribution data but it is no longer needed:")
    for entry in report_present_already:
        if entry not in report_was_required:
            print(entry)

def mark_file_as_missing_clear_attribution(session, watchlisting_session, file_data):
    while True:
        file = file_data['filename']
        try:
            mediawiki_api_wrapper.login_and_editing.watchlist_page(watchlisting_session, file)
        except mediawiki_api_wrapper.login_and_editing.NoEditPermissionException:
            # Recreate session, may be needed after long processing
            watchlisting_session = shared.create_login_session()
        try:
            file_page = mediawiki_api_wrapper.query.download_page_text_with_revision_data(file)
            template = file_data['templates_without_attribution'][0]
            template = str(template)
            text = file_page['page_text'] + "\n" + file_template_about_missing_attribution(template)
            text = fix_template_capitalization(text)
            mediawiki_api_wrapper.login_and_editing.edit_page_and_show_diff(session, file, text, "mark file as missing well specified attribution (in template itself)", file_page['rev_id'], file_page['timestamp'])
            return session, watchlisting_session
        except mediawiki_api_wrapper.login_and_editing.NoEditPermissionException:
            # Recreate session, may be needed after long processing
            session = shared.create_login_session('image_bot')
    raise

def fix_template_capitalization(text):
    # cc-by-sa-2.0 template works, cc-by-sa-2.0-self not
    # this can result in problems like
    # https://wiki.openstreetmap.org/w/index.php?title=File:IMG_1644.JPG&diff=next&oldid=2383093
    text = re.sub('cc-by-sa', 'CC-BY-SA', text, flags=re.IGNORECASE)
    text = re.sub('cc-by-', 'CC-BY-', text, flags=re.IGNORECASE)
    return text

def notify_user_on_their_talk_page(session, uploader, files):
    user_page_title = "User talk:" + uploader
    page = mediawiki_api_wrapper.query.download_page_text_with_revision_data(user_page_title)
    message = get_message(files)
    edit_summary = "notify about missing well specified attribution in uploaded files"
    while True:
        try:
            mediawiki_api_wrapper.login_and_editing.append_text_to_page_and_show_diff(session, user_page_title, "\n" + message, edit_summary)
            return session
        except mediawiki_api_wrapper.login_and_editing.NoEditPermissionException:
            # Recreate session, may be needed after long processing
            session = shared.create_login_session('image_bot')
    raise

def skip_file(page_text, page_title):
    for blocker in ["josm", "-map-", "maps]]",
    "Author: [https://www.openstreetmap.org/user/Szem Szem]", # only question of formatting
    "own work",
    "Présentation", "Presentazione", "Presentation", "Category:Presentations", "Flyer", "Category:Poster", "fair use",
    ]:
        if blocker in page_text.lower()+page_title:
            return True
    if "screenshot" in page_text.lower()+page_title or "images of josm" in page_text.lower():
        # obnoxious and tricky to handle, skipping it, sorry
        return True
    for category_name in ["Rendered OSM data", "Aerial imagery", "Maps", "Overpass turbo",
        'Images of data use permissions, rejections or requests', # likely should not have license template at all
        'Images of published materials related to OpenStreetMap', # the same
    ]:
        if ("Category:" + category_name).lower() in page_text.lower(): # TODO remove this in future or handle specially
            return True
    return False

def is_marked_already(page_text):
    if template_name().lower() in page_text.lower():
        return True
    text_considered_as_marking = [
       "{{Superseded by Commons",
       "{{Unknown",
       "{{Delete",
       "OpenStreetMap trademark", "OpenStreetMap logo", # TODO handle this mess and remove this
    ]
    for text in text_considered_as_marking:
        if text.lower() in page_text.lower():
            return True
    return False

def template_name():
    return "Proper attribution missing"

def selectable_category_name_part():
    mydate = datetime.datetime.now()
    # see https://man7.org/linux/man-pages/man3/strftime.3.html for formatting
    current_month = mydate.strftime("%B") # January
    current_year = mydate.strftime("%Y")
    return "uploader notified " + current_year + ", " + current_month

def entire_category_name():
    return "Media without a proper attribution - " + selectable_category_name_part()

def create_category_for_the_current_month_if_missing(session):
    category_name = "Category:" + entire_category_name()
    category_text = category_for_given_month_page_text()
    data = mediawiki_api_wrapper.query.download_page_text_with_revision_data(category_name)
    if data == None:
        mediawiki_api_wrapper.login_and_editing.create_page_and_show_diff(session, category_name, category_text, "create category for holding files awaiting response")
    elif data['page_text'] != category_text:
        print("DIFFFFFFFFFFFFFFFFF!")
        print(data['page_text'])
        print(category_text)
        mediawiki_api_wrapper.login_and_editing.edit_page_and_show_diff(session, category_name, category_text, "reset category for holding files awaiting response", data['rev_id'], data['timestamp'])

def category_for_given_month_page_text():
    mydate = datetime.datetime.now()
    # see https://man7.org/linux/man-pages/man3/strftime.3.html for formatting
    current_month = mydate.strftime("%m") # 01
    current_year = mydate.strftime("%Y")
    return """Use <pre>""" + file_template_about_missing_attribution("template_name_used_for_file") + """</pre>to put file here.

Please, use solely only where someone who uploaded file was notified during this month.

__HIDDENCAT__
[[Category:Media without a proper attribution|""" + current_year + "-" + current_month  + """]]"""

def file_template_about_missing_attribution(template):
    return "{{" + template_name() + "|template=" + template + "|subcategory=" + selectable_category_name_part() + "}}"

def get_message(list_of_file_infos):
    uploaded = "file"
    to_be = "is"
    description_form = "description"
    if len(list_of_file_infos) >= 2:
        uploaded = "files"
        to_be = "are"
        description_form = "descriptions"
    message = "== Attribution =="
    message += "\n\n"
    message += "Hello! And sorry for bothering you, but " + description_form + " of " + uploaded + " you uploaded need to be improved."
    message += "\n\n"
    message += "You have uploaded " + uploaded + " which " + to_be + " licensed as requiring attribution. But right now attribution is not specified properly."
    message += "\n\n"
    message += "Please, [https://wiki.openstreetmap.org/w/index.php?title=Talk:Wiki&action=edit&section=new ask for help] if something is confusing or unclear in this message."
    message += "\n\n"
    message += "Please, fix that problem with this uploads - note that images with unclear licensing situation may be deleted."
    message += "\n\n"
    message += "Attribution may be missing completely or just be specified in nonstandard way, in either case it needs to be improved. Note that using CC-BY files without specifying attribution is a copyright violation, which is often unethical and unwanted. So clearly specifying required attribution is needed if license which makes attribution mandatory was used."
    message += "\n\n"
    message += "If it is applying to your own work which not based on work by others - then you can select own user name or some other preferred attribution or even change license to for example {{T|CC0-self}}"
    message += "\n\n"
    message += "For files which are solely your own work: ensure that it is clearly stated at file page that you created image/took the photo/etc"
    message += "\n\n"
    message += "For works by others - please ensure that there is link to the original source which confirms license and that you used proper attribution, or that source is clearly stated in some other way. This applies when you took screeshot, made map from OSM data and so on."
    message += "\n\n"
    message += "Especially for old OSM-baded maps, made from data before license change on 12 September 2012 you should use \"map data © OpenStreetMap contributors\" as at least part of attribution"
    message += "\n\n"
    message += "For old [[OSM Carto]] maps, which predate license change on 12 September 2012 you can use a special template <nowiki> {{OSM Carto screenshot||old_license}}</nowiki>"
    message += "\n\n"
    message += "Note: Maybe the current license on this file is wrong and a different one should be used! [[Wiki:Media file license chart]] may be helpful. If unsure, ask on [[Talk:Wiki]]"
    message += "\n\n"
    for file in list_of_file_infos:
        if len(file['templates_without_attribution']) != 1:
            raise
        template_name = str(file['templates_without_attribution'][0])
        message += "* [[:" + file['filename'] + "]]"
        message += "\n"
    return message

def expected_templates():
    returned = []
    for basic in ["CC-BY", "CC-BY-SA"]:
        for version in ["2.0", "3.0", "4.0"]:
            returned.append(basic + "-"  + version)
    return returned

def get_all_affected_uploads_by_user(uploader):
    returned = []
    for page_title in mediawiki_api_wrapper.query.uploads_by_username_generator(uploader):
        #print("get_all_affected_uploads_by_user", uploader, page_title)
        data = mediawiki_api_wrapper.query.download_page_text_with_revision_data(page_title)
        text = data['page_text']
        if is_marked_already(text):
            continue
        if skip_file(text, page_title):
            continue
        missing_attribution = list_templates_with_missing_attribution_for_this_text(text, page_title)
        if len(missing_attribution) > 1:
            print(page_title)
            print(missing_attribution)
            print("giving up")
            print()
        elif len(missing_attribution) == 1:
            returned.append({"filename": page_title, "templates_without_attribution": missing_attribution})
    return returned

def list_templates_with_missing_attribution_for_this_text(text, page_title):
    returned = []
    wikicode = mwparserfromhell.parse(text)
    templates = wikicode.filter_templates()
    for template in templates:
        if template.name.strip().upper() in expected_templates():
            if len(template.params) > 1:
                for param in template.params:
                    print(param)
                raise
            if len(template.params) == 1:
                # TODO: check is it ending at the correct location
                continue
            if len(template.params) == 0:
                if text.replace("{{" + str(template.name) + "}}", "").replace("== Licensing ==", "").strip() != "":
                    print()
                    print()
                    print()
                    print("attribution may be recoverable...")
                    print(mediawiki_api_wrapper.interface.osm_wiki_page_link(page_title))
                    print(text)
                    print()
                returned.append(template.name)
            else:
                raise "impossible"
    return returned

main()
