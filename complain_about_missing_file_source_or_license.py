import wiki_category_and_template_structure
import shared
import password_data

import mediawiki_api_wrapper

import datetime
import random
import time

# TODO: scan recently uploaded
# TODO: scan all problematic from specific user
# https://wiki.openstreetmap.org/wiki/Special:Log/upload
# https://wiki.openstreetmap.org/w/api.php?action=query&list=logevents&letype=upload&lelimit=500
# https://wiki.openstreetmap.org/w/api.php?action=query&list=logevents&letype=upload&lelimit=500&leuser=Marek%20kleciak

def todo():
    raise("""mark_file_as_without_copyright_info_and_notify_user - this should be transformed into stuff expanding file list

   
    detect_images_with_missing_licences should be turned into version expanding to get all files from specifix user if any image applies and getting all problematic images of a specific user
    
    report all problematic images in one go, like complain_about_missing_attribution_where_license_requires_it is doing to irritate people less - generalize complain_about_missing_attribution_where_license_requires_it.py instead and use it? simplify code here so change is easy? 
    
    Write to some already notified people

    Rebuild concentrates in detect_images_with_missing_licences

    Write function collecting all this data for specific user (look where detect_images_with_missing_licences is used - it exists already!)
    Start processing uploader by uploader
    Deprecate detect_images_with_missing_licences
    Write new one returning by groups, one user in each 
    Write new preview page
    Run code that exists without spamming anuone
    """)

def main():
    selftest()
    session = shared.create_login_session()

    shared.ensure_existence_category_for_this_month_notification(session)
    skipped_users = shared.users_dropped_from_regular_processing()
    refresh_users = [
    # see https://wiki.openstreetmap.org/wiki/User:Mateusz_Konieczny/notify_uploaders/Fredao
    "Markus B", "PanierAvide", "Segubi", "Reneman", "marek kleciak", "Fredao", "Fkv", "SimonPoole",
    ]
    not_critical_maintenance(skipped_users, refresh_users) # not affected by TODO below, later remove it
    todo()
    manual_scripted_image_edits()

    sources = sources_of_images_for_checking()
    complain_about_images(skipped_users, sources[0])
    for source in sources[1:]:
        complain_about_images(skipped_users, source)

    edit_images_in_categories_as_missing_license_data(session, wiki_category_and_template_structure.screeshot_categories(), limit=90)
    edit_images_in_categories_as_missing_license_data(session, wiki_category_and_template_structure.map_categories(), limit=90)

    # for 6+month old and marked as waiting for action for uploader
    # {{delete|unused image, no evidence of free licensing, unused so not qualifying for fair use}}

def not_critical_maintenance(skipped_users, refresh_users):
    bot_session = shared.create_login_session('image_bot')
    edit_images_in_categories_as_missing_license_data(bot_session, wiki_category_and_template_structure.screeshot_categories(), limit=300)
    edit_images_in_categories_as_missing_license_data(bot_session, wiki_category_and_template_structure.map_categories(), limit=300)
    action_done_after_the_first_source_are_still_waiting = False
    for user in skipped_users + refresh_users:
        returned = make_page_listing_problematic_uploads_by_user(bot_session, user)
        bot_session = returned['session']

def manual_scripted_image_edits():
    session = shared.create_login_session()
    bot_session = shared.create_login_session('image_bot')
    screenshot_only_uploads_remaining = [
    ]
    for user in screenshot_only_uploads_remaining:
        mark_all_unmarked_files_by_user(session, user, "{{unknown}}\n[[Category:Screenshots]]")
    maps_only_uploads_remaining = [
    ]
    for user in maps_only_uploads_remaining:
        mark_all_unmarked_files_by_user(session, user, "{{unknown}}\n[[Category:Maps]]")
    """
    make_page_listing_problematic_uploads_by_user(bot_session, "Paysages")
    mark_all_unmarked_files_by_user(session, "UNGSC-DTLM-Ale Zena", "{{Unknown|subcategory=uploader notified 2022, August}}")
    mark_all_unmarked_files_by_user(session, "Berteun", "[[Category:Screenshots]]\n{{unknown}}")
    mark_all_unmarked_files_by_user(session, "Davalv", "[[Category:Maps]]\n{{unknown}}")
    mark_all_unmarked_files_by_user(session, "Mikel", "{{CC-BY-SA-2.0 OpenStreetMap}}\n[[Category:Maps]]")
    mark_all_unmarked_files_by_user(session, "R0uzic", "{{PD-shape}}")

    days_of_inactive_talk_page = 40
    complain_about_missing_file_source_or_license(count_of_edited_files=77, extra_files_to_preview=88, files_for_processing=['File:Rotwein.png'], banned_users=skipped_users, source_description="single file", days_of_inactive_talk_page=0)

    added = "{{PD-shape}}"
    category = "Category:Openfietskaart Icon"
    for page_title in mediawiki_api_wrapper.query.pages_from_category(category):
        test_page = mediawiki_api_wrapper.query.download_page_text_with_revision_data(page_title)
        if wiki_category_and_template_structure.is_marked_with_template_declaring_licensing_status(test_page['page_text']):
            continue
        text = test_page['page_text'] + "\n" + added
        mediawiki_api_wrapper.login_and_editing.edit_page_and_show_diff(session, page_title, text, "", test_page['rev_id'], test_page['timestamp'])
    """

def selftest():
    wiki_category_and_template_structure.license_selfcheck()
    text = """{{unknown}}
[[category:Images]]
"""
    if skip_image_based_on_text_on_presence_of_keywords_in_description("dummy", text) != False:
        raise Exception("wrong classification")
    if skip_image_based_on_text_on_its_description("dummy", text) != False:
        raise Exception("wrong classification")
    text = "{{delete|unused duplicate of https://wiki.openstreetmap.org/wiki/File:Rotwein.png}}"
    if skip_image_based_on_text_on_its_description("dummy", text) != True:
        raise Exception("wrong classification")
    text = """
    == License ==
    {{Unknown}}

    [[Category:Man made icons]]
    """
    if skip_image_based_on_text_on_its_description("testing", text):
        raise Exception("wrong removal of nonlicensing code")

    if skip_image_based_on_text_on_its_description("testing", "{{OpenStreetMap trademark}}") == False:
        # OSM logo licensing is tricky at best, lets ignore it
        raise Exception("{{OpenStreetMap trademark}} should skip processing image")

    # TODO: eliminate that template
    # https://wiki.openstreetmap.org/wiki/Special:WhatLinksHere/Template:Insignia
    if skip_image_based_on_text_on_its_description("testing", "{{Insignia}}"):
        raise Exception("wrong removal of nonlicensing code")

def sources_of_images_for_checking():
    sources = []
    sources.append({"description": "marked with template declaring lack of license", "files": mediawiki_api_wrapper.query.pages_from_category("Category:Media without a license - uploader not notified")})
    sources.append({"description": "without any category", "files": uncategorized_images_skipping_some_initial_ones()})
    sources.append({"description": "by date, from 2009", "files": mediawiki_api_wrapper.query.images_by_date("2009-01-01T18:05:46Z")})
    sources.append({"description": "by date, from start", "files": mediawiki_api_wrapper.query.images_by_date("1900-01-01T18:05:46Z")})
    sources.append({"description": "by date, from 2016", "files": mediawiki_api_wrapper.query.images_by_date("2016-01-01T18:05:46Z")})
    sources.append({
        "description": "by date, from 2022 - [[Special:Uploads]]",
        "files": mediawiki_api_wrapper.query.images_by_date("2022-01-01T00:00:00Z"),
        "days_of_inactive_talk_page": 0,
        })
    random.shuffle(sources)
    return sources


def complain_about_images(skipped_users, source):
    print(source["description"])
    days_of_inactive_talk_page = 20
    if 'days_of_inactive_talk_page' in source:
        days_of_inactive_talk_page = source['days_of_inactive_talk_page']
    complain_about_missing_file_source_or_license(count_of_edited_files=21, extra_files_to_preview=23, files_for_processing=source["files"], banned_users=skipped_users, source_description=source["description"], days_of_inactive_talk_page=days_of_inactive_talk_page)

def edit_images_in_categories_as_missing_license_data(session, processed_categories, limit=10):
    for category in processed_categories:
        for page_title in mediawiki_api_wrapper.query.pages_from_category(category):
            if page_title in wiki_category_and_template_structure.screeshot_categories():
                continue
            if "Category:" in page_title:
                print("Skipping", page_title, "as category")
                print("        \"" + page_title + "\",")
                # TODO: run on subcategories?
                continue
            if "File:" not in page_title:
                print("Skipping", page_title, "as without File: in the title")
                continue
            test_page = mediawiki_api_wrapper.query.download_page_text_with_revision_data(page_title)
            if wiki_category_and_template_structure.has_license_marking_as_without_source(test_page['page_text']):
                continue
            if wiki_category_and_template_structure.is_marked_with_template_declaring_licensing_status(test_page['page_text']):
                continue
            if "{" in test_page['page_text']:
                print("has template, without declaring licensing status!")
            print(page_title)
            text = test_page['page_text'] + "\n" + "{{" + wiki_category_and_template_structure.primary_no_license_template() + "}}"

            while True:
                try:
                    mediawiki_api_wrapper.login_and_editing.edit_page_and_show_diff(session, page_title, text, "what is the license here? (please fill it if you know it or you are author and can license it! [[Wiki:Media file license chart]] may be helpful)", test_page['rev_id'], test_page['timestamp'])
                    break
                except mediawiki_api_wrapper.login_and_editing.NoEditPermissionException:
                    # Recreate session, may be needed after long processing
                    session = shared.create_login_session()
            limit -= 1
            print("remaining limit:", limit)
            if limit <= 0:
                return
            shared.make_delay_after_edit()

def uncategorized_images_skipping_some_initial_ones():
    skip = random.randrange(0, 5000)
    print("skipping", skip)
    skipped = []
    for file in mediawiki_api_wrapper.query.uncategorized_images():
        if len(skipped) < skip:
            skipped.append(file)
        else:
            yield file
    for file in skipped:
        yield file

def mark_all_unmarked_files_by_user(session, username, marker):
    generated_data = problematic_files_from_specific_uploader(username)
    for entry in generated_data:
        user_page_title = entry['page_title']
        test_page = mediawiki_api_wrapper.query.download_page_text_with_revision_data(user_page_title)
        edit_summary = "marking as " + marker
        mediawiki_api_wrapper.login_and_editing.append_text_to_page_and_show_diff(session, user_page_title, "\n" + marker, edit_summary)

def make_page_listing_problematic_uploads_by_user(bot_session, username, minimum=2):
    if bot_session == None:
        raise Exception("session cannot be None")
    generated_data = problematic_files_from_specific_uploader(username)
    page_name = "User:" + password_data.username() + "/notify uploaders/" + username
    bot_session = show_overview_page(bot_session, generated_data, page_name, limit, username + " [[Drafts/Media file license chart]]", minimum_for_new_page=2)
    if bot_session == None:
        raise Exception("session cannot be None")
    return {"page_name": page_name, "problematic_image_data": generated_data, 'session': bot_session}

def show_overview_page(bot_session, generated_data, show_page, break_after, hint, minimum_for_new_page):
    if bot_session == None:
        raise Exception("session cannot be None")
    test_page = mediawiki_api_wrapper.query.download_page_text_with_revision_data(show_page)
    if test_page == None:
        if len(generated_data) < minimum_for_new_page:
            return bot_session
    table_for_confirmation = shared.generate_table_showing_image_data_for_review(generated_data, break_after=break_after)
    text = hint + "\n" + table_for_confirmation
    edit_summary = "copyright review"
    while True:
        try:
            if test_page == None:
                mediawiki_api_wrapper.login_and_editing.create_page(bot_session, show_page, text, edit_summary, mark_as_bot_edit=True)
                return bot_session
            elif test_page['page_text'] != text:
                mediawiki_api_wrapper.login_and_editing.edit_page(bot_session, show_page, text, edit_summary, test_page['rev_id'], test_page['timestamp'], mark_as_bot_edit=True)
                return bot_session
        except mediawiki_api_wrapper.login_and_editing.NoEditPermissionException:
            # Recreate session, may be needed after long processing
            bot_session = shared.create_login_session('image_bot')
    raise "unexpected"

def complain_about_missing_file_source_or_license(count_of_edited_files, extra_files_to_preview, files_for_processing, banned_users, source_description, days_of_inactive_talk_page):
    session = shared.create_login_session()
    if session == None:
        raise Exception("session cannot be None")
    generated_data = detect_images_with_missing_licences(count_of_edited_files + extra_files_to_preview, files_for_processing, banned_users, notify_uploaders_once=True, days_of_inactive_talk_page=days_of_inactive_talk_page)

    cleaned_generated_data = []
    for data in generated_data[:count_of_edited_files]:
        if data["uploader"] in banned_users:
            continue
        info = problematic_files_from_specific_uploader(data["uploader"])
        if len(info) > 1:
            print("skipping", data["uploader"])
            print(info)
            continue
        cleaned_generated_data.append(data)
    print("reduced", len(generated_data), "to", len(cleaned_generated_data))
    generated_data = cleaned_generated_data

    bot_session = shared.create_login_session('image_bot')
    bot_session, generated_data = create_overview_pages_for_users_with_more_problematic_uploads(bot_session, generated_data)
    show_page = "User:" + password_data.username() + "/test"

    # datetime.datetime.strptime('2021-04-19T18:22:40Z', "%Y-%m-%dT%H:%M:%SZ")
    hint = wikicode_about_useful_templates_for_processing_files() + "\n\nsource of files: " + source_description + "\n\n"
    bot_session = show_overview_page(bot_session, generated_data, show_page, count_of_edited_files, hint, minimum_for_new_page=0)

    if len(generated_data) == 0:
        return

    mediawiki_api_wrapper.interface.show_latest_diff_on_page(show_page)
    print("Launch marking of files, as presented on", show_page, "?")
    shared.pause()
    for data in generated_data[:count_of_edited_files]:
        page_title = data['page_title']
        print("page title:", page_title)
        edit_summary = "please, specify missing information about file that would allow keeping it on OSM Wiki"
        session = mark_file_as_without_copyright_info_and_notify_user(session, data, edit_summary)

def detect_images_with_missing_licences(files_to_find_count, files_for_processing, banned_users=[], notify_uploaders_once=True, notify_recently_notified=False, days_of_inactive_talk_page=40):
    # returns list
    generated_data = []

    reported_remaining_count = files_to_find_count
    processed_images = []
    for page_title in files_for_processing:
        if page_title in processed_images:
            continue
        processed_images.append(page_title)
        returned = detect_images_with_missing_licences_process_file(page_title, banned_users)
        if returned == None:
            continue
        if notify_recently_notified == False:
            if is_uploader_eligible_for_new_complaints(returned['uploader'], days_of_inactive_talk_page) == False:
                print("Skip", returned['uploader'], "as they are ineligible for new complaints")
                banned_users.append(returned['uploader'])
                continue
        generated_data.append(returned)
        if notify_uploaders_once:
            banned_users.append(returned['uploader'])
        reported_remaining_count -= 1
        if reported_remaining_count <= 0:
            break
    return generated_data


# use returned session, it could be renewed
def mark_file_as_without_copyright_info_and_notify_user(session, data_about_affected_page, edit_summary):
    data = data_about_affected_page
    current_data = mediawiki_api_wrapper.query.download_page_text_with_revision_data(data['page_title'])
    if data['page_text'] != current_data['page_text']:
        print(data['page_title'], "contents were changed in meantime, skipping")
        return session
    try:
        notify_user_about_missing_copyright_data(session, data['uploader'], data['page_title'], edit_summary)
    except mediawiki_api_wrapper.login_and_editing.NoEditPermissionException:
        # Recreate session, may be needed after long processing
        session = shared.create_login_session()
        notify_user_about_missing_copyright_data(session, data['uploader'], data['page_title'], edit_summary)

    try:
        edit_talk_page_to_mark_uploader_as_notified(session, data['page_title'], data['page_text'], edit_summary, data['rev_id'], data['timestamp'])
    except mediawiki_api_wrapper.login_and_editing.NoEditPermissionException:
        # Recreate session, may be needed after long processing
        session = shared.create_login_session()
        edit_talk_page_to_mark_uploader_as_notified(session, data['page_title'], data['page_text'], edit_summary, data['rev_id'], data['timestamp'])
    return session

def notify_user_about_missing_copyright_data(session, uploader_name, page_title, edit_summary):
    user_talk = "User talk:" + uploader_name
    notification = notification_on_user_talk(page_title, uploader_name)
    mediawiki_api_wrapper.login_and_editing.append_text_to_page_and_show_diff(session, user_talk, notification, edit_summary)

def edit_talk_page_to_mark_uploader_as_notified(session, page_title, page_text, edit_summary, rev_id, timestamp):
    for template in wiki_category_and_template_structure.templates_marking_as_without_source():
        page_text = page_text.replace("{{" + template.lower() + "}}", "").replace("{{" + template + "}}", "")
    if(wiki_category_and_template_structure.has_license_marking_as_without_source(page_text)):
        raise "unexpected"
    page_text = page_text + "\n" + shared.file_template_about_missing_license()
    mediawiki_api_wrapper.login_and_editing.edit_page_and_show_diff(session, page_title, page_text, edit_summary, rev_id, timestamp)

def notification_on_user_talk(image_name, uploader_name):
    # https://wiki.openstreetmap.org/wiki/Category:Media_without_a_license#Writing_to_an_uploader
    # NOTE! one layer of nowiki should be removed!
    # NOTE! line
    # Once you add missing data - please remove <nowiki>""" + shared.file_template_about_missing_license() + """</nowiki> from the file page.
    # is added only here as exact template can be dynamically generated
    return """
== Missing file information ==
Hello! And thanks for your upload - but some extra info is necessary.

Are you the creator of image [[:""" + image_name + """]] ?

Or is it copied from some other place (which one?)?

Please, add this info to the file page - something like "I took this photo" or "downloaded from -website link-" or "I took this screeshot of program XYZ" or "this is map generated from OpenStreetMap data and SRTM data" or "map generated from OSM data and only OSM data" or "This is my work based on file -link-to-page-with-that-file-and-its-licensing-info-" or "used file downloaded from internet to create it, no idea which one".

Doing this would be already very useful.

=== Licensing - photos ===
In case that you are the author of the image: Would you agree to open licensing of this image, allowing its use by anyone (similarly to your OSM edits)?

In case where it is a photo you have taken then you can make it available under a specific free license (except some cases, like photos of modern sculptures in coutries without freedom of panorama or taking photo of copyrighted artwork).

Would you be OK with CC0 (it allows use without attribution or any other requirement)?

Or do you prefer to require attribution and some other things using CC-BY-SA-4.0? 

If you are the author: Please add <nowiki>{{CC0-self}}</nowiki> to the file page to publish the image under CC0 license.

You can also use <nowiki>{{CC-BY-SA-4.0-self|""" + uploader_name + """}}</nowiki> to publish under CC-BY-SA-4.0 license.

Once you add missing data - please remove <nowiki>""" + shared.file_template_about_missing_license() + """</nowiki> from the file page.

=== Licensing - other images ===

If it is not a photo situation gets a bit more complicated.

See [[Drafts/Media file license chart]] that may help.

note: if you took screenshot of program made by someone else, screenshot of OSM editor with aerial imagery: then licensing of that elements also matter and you are not a sole author.

note: If you downloaded image made by someone else then you are NOT the author.

Note that in cases where photo is a screenshot of some software interface: usually it is needed to handle also copyright of software itself.

Note that in cases where aerial imagery is present: also licensing of an aerial imagery matter.

=== Help ===

Feel free to ask for help if you need it - you can do it for example by asking on [[Talk:Wiki]]: [https://wiki.openstreetmap.org/w/index.php?title=Talk:Wiki&action=edit&section=new new topic].

Please ask there if you are not sure what is the proper next step. Especially when you are uploading files that are not your own work or are derivative work (screenshots, composition of images, using aerial imagery etc).

If you are interested in wider discussion about handling licencing at OSM Wiki, see [https://wiki.openstreetmap.org/wiki/Talk:Wiki#Designing_policy_for_handling_files_without_clear_license this thread].

--~~~~

"""

def problematic_files_from_specific_uploader(username):
    files_for_processing = mediawiki_api_wrapper.query.uploads_by_username_generator(username)
    processed_images = []
    for page_title in files_for_processing:
        if page_title in processed_images:
            continue
        processed_images.append(page_title)
        returned = detect_images_with_missing_licences_process_file(page_title, [])
        if returned == None:
            continue
        generated_data.append(returned)
    return generated_data



def wikicode_about_useful_templates_for_processing_files():
    return """For help with dealing with unlicensed media, see https://wiki.openstreetmap.org/wiki/Category:Media_without_a_license and [[Drafts/Media file license chart]]
<br>
<nowiki>{{JOSM screenshot without imagery}}</nowiki>
<br>
{{T|JOSM screenshot without imagery|no_osm_data}}
<br>
{{T|JOSM screenshot without imagery|old_license}}
<br>
{{T|JOSM screenshot with imagery}}
<br>
{{T|JOSM screenshot with imagery|{{T|Bing image}} }}
<br>
{{T|JOSM screenshot without imagery|old_license}}
<br>
{{T|OSM Carto screenshot}}
<br>
{{T|OSM Carto screenshot||old_license}}
<br>
{{T|OpenStreetMap trademark}}
<br>
{{T|CC-BY-SA-2.0 OpenStreetMap}} (uploaded before September 12, 2012)
<br>
{{T|Bing image}}
<br>
{{T|iD screenshot}}
<br>
<hr>
<br>
<nowiki>
{{""" + shared.file_template_about_missing_license() + """}}
</nowiki>
<br>
<hr>
<br>"""

# use returned session, it could be renewed
def create_overview_pages_for_users_with_more_problematic_uploads(bot_session, generated_data):
    if bot_session == None:
        raise Exception("session cannot be None")
    for entry in generated_data:
        print("listing requested for", entry["uploader"])
        info = make_page_listing_problematic_uploads_by_user(bot_session, entry["uploader"], minimum=2)
        bot_session = info['session']
        if len(info["problematic_image_data"]) > 1:
            print("user", entry["uploader"], "has more problematic images")
        entry['more_problematic_images'] = info["problematic_image_data"]
        print("listing completed for", entry["uploader"])
    if bot_session == None:
        raise Exception("session cannot be None")
    return bot_session, generated_data

def is_uploader_eligible_for_new_complaints(uploader, days_of_inactive_talk_page):
    user_talk_data = mediawiki_api_wrapper.query.download_page_text_with_revision_data("User talk:"+uploader)
    if user_talk_data != None:
        person_badgering = password_data.username()
        if user_talk_data['page_text'].find(person_badgering) != -1:
            timestamp = mediawiki_api_wrapper.query.parse_mediawiki_time_string(user_talk_data['timestamp'])
            now = datetime.datetime.now()
            days_since_last = (now - timestamp).days
            if days_since_last < days_of_inactive_talk_page:
                print("------------------")
                print("uploader (" + uploader + ") received comments, from " + person_badgering + " - and their talk page was edited within last", days_since_last, "days")
                # prefer broad range of notifications in initial stage
                return False
    return True


def detect_images_with_missing_licences_process_file(page_title, banned_users):
    """
    return None if image is image is skipped for some reason
        - also when it has template providing license info
    if image is problematic returns dictionary looking more or less like this:
        {'page_title': page_title,
        'page_text': page_text,
        'uploader': uploader,
        'rev_id': page_data['rev_id'],
        'parent_id': page_data['parent_id'],
        'timestamp': page_data['timestamp']
        }
    """
    unused = True
    if is_skipped_file_type(page_title):
        return None

    # TODO: return also metadata here (po oddaniu)
    page_data = mediawiki_api_wrapper.query.download_page_text_with_revision_data(page_title)

    if page_data == None:
        print("none? here? For page_data?")
        print(page_title)
        return None

    page_text = page_data['page_text']

    if page_text == None:
        print("none? here? For page_text?")
        print(page_title)
        return None

    if skip_image_based_on_text_on_its_description(page_title, page_text):
        return None

    upload_history = mediawiki_api_wrapper.query.file_upload_history(page_title)
    uploader = shared.get_uploader_from_upload_history_or_none_if_not_clear(upload_history, page_title, log_when_returned_none_due_to_multiple_uploaders=True)
    if uploader == None:
        return None
    if uploader in banned_users:
        return None
    #print("-----------file upload history-------------")
    #print("-------------------------------------------")
    returned = {'page_title': page_title,
            'page_text': page_text,
            'uploader': uploader,
            'rev_id': page_data['rev_id'],
            'parent_id': page_data['parent_id'],
            'timestamp': page_data['timestamp']
            }
    upload_time = mediawiki_api_wrapper.query.get_upload_date_from_file_history(upload_history)
    if upload_time != None:
        returned['upload_time'] = upload_time
    else:
        print("https://wiki.openstreetmap.org/wiki/"+page_title.replace(" ", "_"))
    return returned

def nonlicensing_wikicode():
    returned = ["[[Category:Software design images]]",
    "[[category:plots and charts]]",
    "{{Insignia}}",
    "{{Tag|", "{{Key|", "{{Tagvalue|", # link tags
    "{{itas|", # pointless templates to link external sites
    "{{Information|", # does not link by itself
    # "{{Trademarked}}" - requires fair use policy
    "{{bing image portions}}", # not a license by itself I think
    "{{featured date", "{{featured_date",
    "http://wiki.openstreetmap.org/wiki/Category:Life_Long_Learning_Mapping_Project",
    "== License ==", "== Licence ==", # often used above "unknown" template
    ]
    for template in wiki_category_and_template_structure.templates_marking_as_without_source():
        for prefix in ["", "== License ==\n"]:
            returned.append(prefix + "{{" + template + "}}")
            returned.append(prefix + "{{" + template.lower() + "}}")
    return returned

def skip_image_based_on_text_on_its_description(page_title, page_text):
    if wiki_category_and_template_structure.is_marked_with_template_declaring_licensing_status(page_text):
        return True
    if page_text.find("#REDIRECT[[") == 0:
        return True # it is redirect, not an actual file
    cleaned_text = remove_text_from_image_description_which_is_not_declaring_license(page_text)
    if "{" in cleaned_text.strip() != "":
        #TODO stop skipping here
        return True
    return skip_image_based_on_text_on_presence_of_keywords_in_description(page_title, cleaned_text)

def remove_text_from_image_description_which_is_not_declaring_license(page_text):
    cleaned_text = page_text.lower()
    cleaned_text = cleaned_text.replace("{{template:", "{{") # overly verbose by valid method of template use
    for remove in nonlicensing_wikicode():
        cleaned_text = cleaned_text.replace(remove.lower(), "")
    return cleaned_text

def skip_image_based_on_text_on_presence_of_keywords_in_description(page_title, cleaned_text):
    keywords = wiki_category_and_template_structure.screeshot_categories() + [
    # redirect (TODO: consider parsing)
    "#REDIRECT",
    "{{OpenStreetMap trademark}}", "trademark", "logo", # unlikely to be photos
    "[[Category:Logos]]", # likely {{trademarked}} is missing which would cause skip anyway
    "JOSM", # likely {{JOSM screenshot without imagery}} or one for with imagery
    "OSM picture", "Rendered OSM data", "Category:Maps of places", "Map of", "OSM-Daten",
    "taghistory", "chart", "graph", "Category:Statistics",
    "slippy map", "map", "mapping party",
    "Category:Aerial imagery", "Aerial imagery" # requires custom explanation TODO - maybe start providing it
    "https://wiki.openstreetmap.org/wiki/File:Mary_blue_hu.png" # as https://wiki.openstreetmap.org/wiki/File:Mary_blue_hu.png has unclear license, anything based on it is also going to be unclear TODO - review it in 2024
    # partially explains the source
    ] + shared.description_keywords_that_may_indicate_that_author_is_specified() + [
    "http",
    "UN UNIS Satellite Imagery", # https://wiki.openstreetmap.org/wiki/User_talk:UNGSC-DTLM-Ale_Zena
    "non-free", "image search", "unfree",
    "CC-BY-SA", "CC-BY", "public domain",
    "fair use", "copyright",
    "licence", "license", "permission", "Openfietsmap",
    # commons, wikipedia, 0px- covers cases like
    # https://wiki.openstreetmap.org/wiki/File:120px-Zusatzzeichen_1020-12.svg.png https://commons.wikimedia.org/w/index.php?title=File:Zusatzzeichen_1020-12.svg&redirect=no
    # where bothering uploader is not needed and matches can be automatically found

    # unlikely to be photos and complex
    "StreetComplete", # solve this!
    "Screenshot", # long backlog of weird cases

    # https://wiki.openstreetmap.org/wiki/Category:Documents
    "Category:Documents",
    # https://wiki.openstreetmap.org/wiki/Category:Images_of_data_use_permissions,_rejections_or_requests
    "Category:Images of data use permissions, rejections or requests",
    # subcategories:
    # https://wiki.openstreetmap.org/wiki/Category:ES:Autorizaciones_para_usar_fuentes_de_datos_de_Espa%C3%B1a
    "Category:ES:Autorizaciones para usar fuentes de datos de España",
    "Letter of authorization", "Autorizzazione", "Authorization", # TODO: just use category from above
    "Photo for profile", "profile picture", "profile", "self photo", "taken by", '[[Category:User images]]',
    "Bus.meran.eu", # https://wiki.openstreetmap.org/wiki/File:Bus.meran.eu_real_time_bus_map.png
    "AEP - Captage eau.JPG", # asked on https://wiki.openstreetmap.org/wiki/User_talk:Penegal for now
    "Asked for more info at", "github.com",
    # https://wiki.openstreetmap.org/wiki/Category:Images_of_published_materials_related_to_OpenStreetMap
    "Category:Images of published materials related to OpenStreetMap",
    "Artikel in", "article in", "article about", "News published in local paper", "News published", # fair use?
    "OSM Coverage",

    # https://wiki.openstreetmap.org/wiki/Talk:Drafts/Media_file_license_chart#More_likely_layers%3A_CyclOSM
    "mapquest", "osmarender", "Render cycle", "Render transport", "CyclOSM", "OpenCycleMap", "ÖPNVkarte", # arghhhhhhhhhh, licensing of one more thingy https://wiki.openstreetmap.org/wiki/File:Render_cycle_leisure_playground_node.png https://wiki.openstreetmap.org/wiki/File:Render_transport_leisure_playground_area.png https://wiki.openstreetmap.org/wiki/File:Render_mapquest_leisure_playground_area.png 
    "Potlatch", # licensing of https://wiki.openstreetmap.org/wiki/File:Connected_Ways_in_Potlatch.png
    "freemap", # https://github.com/FreemapSlovakia/freemap-mapnik/issues/237 https://wiki.openstreetmap.org/wiki/File:OSM-Bratislava-2014-08-18.png https://github.com/matkoniecz/mediawiki_file_copyright_handler_bot/commit/d2cf743317a6c7878c5f3c71141b47d28e19d035
    "collage", "Comparing", "based on", # handle in the second wave after other images are processed TODO
    "osmfr", "openstreetmap.fr", "french-style", "french style", # https://github.com/cquest/osmfr-cartocss https://wiki.openstreetmap.org/wiki/File:Mont-Blanc-french-style.png https://wiki.openstreetmap.org/wiki/Drafts/Media_file_license_chart
    "tiles@home", "OSM Carto", "Carto",
    "[[Category:Yahoo! Aerial Imagery]]",

    # sheduled for deletion anyway
    "should be replaced with",

    # TODO enable later - but for now lets skip derivative works
    "derivative work", "version of", "Russian version", "crop of", "extracted from",
    ]

    for keyword in keywords:
        if keyword.lower() in page_title.lower() or keyword.lower() in cleaned_text.lower():
            # TODO drop exception
            print("skipping as likely of historic interest or at least on topic (", keyword, ")", "https://wiki.openstreetmap.org/wiki/"+page_title.replace(" ", "_"))
            return True
    return False

def is_skipped_file_type(page_title):
    if ".doc" in page_title.lower():
        return True # TODO, enable
    if ".ods" in page_title.lower():
        return True # TODO, enable
    if ".odt" in page_title.lower():
        return True # TODO, enable
    if ".pdf" in page_title.lower():
        return True # TODO, enable
    if ".svg" in page_title.lower():
        # skipped as many as are actually below TOO
        return True # TODO, enable
    return False


main()