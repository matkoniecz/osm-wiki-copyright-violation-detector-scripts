import shared

import mediawiki_api_wrapper
import mwparserfromhell

import datetime
import time

def selftest():
    fixed = add_uploader_info("{{CC-BY-4.0-self}}", "uploader")
    if fixed != "{{CC-BY-4.0-self|1=uploader}}":
        print(fixed)
        raise

def main():
    selftest()
    session = shared.create_login_session('image_bot')

    banned_users = shared.users_dropped_from_regular_processing()
    for page_title in mediawiki_api_wrapper.query.pages_from_category("Category:Attribution not provided in CC license template which requires attribution"):
        uploader = shared.get_uploader_of_file_or_none_if_not_clear(page_title)
        if uploader == None:
            continue
        data = mediawiki_api_wrapper.query.download_page_text_with_revision_data(page_title)
        text = data['page_text']
        text = add_uploader_info(text, uploader)
        if text == data['page_text']:
            if "-self" not in data['page_text']:
                continue
            print(page_title, "- no changes proposed")
            print(data['page_text'])
            continue
        print(text)
        print(data['page_text'])
        while True:
            try:
                mediawiki_api_wrapper.login_and_editing.edit_page_and_show_diff(session, page_title, text, "add attribution directly to the template (automatic bot edit based on already listed info)", data['rev_id'], data['timestamp'], mark_as_bot_edit=True)
                break
            except mediawiki_api_wrapper.login_and_editing.NoEditPermissionException:
                # Recreate session, may be needed after long processing
                session = shared.create_login_session()

def add_uploader_info(text, uploader):
    if "{{OpenStreetMap trademark}}".lower() in text.lower():
        return text # not dealing with this, OSM logo has unclear license
    wikicode = mwparserfromhell.parse(text)
    templates = wikicode.filter_templates()
    author = uploader
    for template in templates:
        if template.name.strip().title() == "Information":
            for param in template.params:
                key = param.split("=")[0].strip()
                if "=" not in param:
                    continue
                value = param.split("=")[1].strip()
                if key == "author" and value != "":
                    author = value
                    if author.lower() in ["osm contributors", "unknown", "?", "??"]:
                        return
                    if "?" in author:
                        return
                    for template_inner_run in templates:
                        canonical = canonical_self_template_name(template_inner_run.name)
                        # add based on Information template
                        # like https://wiki.openstreetmap.org/w/index.php?title=File:CSB_II_Ambohitrova.jpg&diff=2389538&oldid=2389306
                        if is_name_matching_expected_self_template(template_inner_run.name):
                            replaced = "{{" + str(template_inner_run.name) + "}}"
                            new = "{{" + canonical + "|1=" + author + "}}"
                            text = text.replace(replaced, new)
                        if is_name_matching_expected_template(template_inner_run.name):
                            replaced = "{{" + str(template_inner_run.name) + "}}"
                            new = "{{" + canonical + "|1=" + author + "}}"
                            text = text.replace(replaced, new)

    for template in templates:
        if is_name_matching_expected_self_template(template.name):
            if len(template.params) == 0:
                replaced = "{{" + str(template.name) + "}}"
                new = "{{" + template.name.strip().upper().replace("-SELF", "-self") + "|1=" + author + "}}"
                text = text.replace(replaced, new)


    return text

def canonical_self_template_name(name):
    return name.strip().upper().replace("-SELF", "-self")

def canonical_template_name(name):
    return name.strip().upper()

def is_name_matching_expected_self_template(name):
    return canonical_self_template_name(name) in expected_self_templates()

def is_name_matching_expected_template(name):
    return canonical_template_name(name) in expected_templates()


def expected_self_templates():
    returned = []
    for template in expected_templates():
        returned.append(template + "-self")
    return returned

def expected_templates():
    returned = []
    for basic in ["CC-BY", "CC-BY-SA"]:
        for version in ["2.0", "3.0", "4.0"]:
            returned.append(basic + "-"  + version)
    return returned


main()
