import mediawiki_api_wrapper
import shared
import mwparserfromhell
import datetime
import password_data

def main():
    session = shared.create_login_session('image_bot')
    for page_title in mediawiki_api_wrapper.query.pages_from_category("Category:Pages with broken file links"):
        if page_title.find("User:" + password_data.username() + "/notify uploaders/") == 0:
            page_text = ""
            edit_summary = "[[:Category:Pages with broken file links]] maintenance - blank draft pages in userspace"
            mediawiki_api_wrapper.login_and_editing.replace_text_on_page_and_show_diff(session, page_title, page_text, edit_summary, mark_as_bot_edit=True)
    # TODO: do it for all generate pages from time to time to avoid generation of future cleanup?
    # "User:" + password_data.username() + "/notify uploaders/"
    # list pages with given prefix
    # https://www.mediawiki.org/wiki/API:Prefixsearch
    # requires new functionality in mediawiki_api_wrapper, I guess
    # maybe do it at the end only for pages unmodified in some time? or for all?
    # probably it would be better to avoid regenerating them from time to time
    # or maybe only say once a year? and avoid blanking pages edited in last year?

main()
