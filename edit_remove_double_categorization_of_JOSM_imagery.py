import shared
import mediawiki_api_wrapper

# https://wiki.openstreetmap.org/wiki/Category:Images_of_JOSM
# TODO: process them, script below is only beginning 
#
"""
{{JOSM screenshot without imagery}}
"""
# if actually screens of Windows
"""
{{delete|unused screenshot of proprietary software (Windows), unused so not qualifying even for fair use}}
"""
# If has aerial imagery
"""
{{JOSM screenshot with imagery|
{{Unknown|subcategory=uploader notified 2022, June}}
}}
"""
"""
What is the source of aerial imagery here?

What is the source and license of aerial imagery used on this images?

*
*
*
*

~~~~
"""

for page_title in mediawiki_api_wrapper.query.pages_from_category("Category:Images of JOSM"):
    session = shared.create_login_session()
    page = mediawiki_api_wrapper.query.download_page_text_with_revision_data(page_title)
    if "{{JOSM screenshot without imagery}}" in page['page_text']:
        text = page['page_text'].replace("[[Category:Images of JOSM]]", "")
        text = page['page_text'].replace("[[category:Images of JOSM]]", "")
        mediawiki_api_wrapper.login_and_editing.edit_page_and_show_diff(session, page_title, text, "remove double categorization (there is already [[:Category:JOSM screenshots without imagery]])", page['rev_id'], page['timestamp'])
