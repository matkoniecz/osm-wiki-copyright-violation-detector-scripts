import mediawiki_api_wrapper
import sqlite3

# https://commons.wikimedia.org/wiki/Help:FastCCI would be better if it would reliably work

# https://commons.wikimedia.org/wiki/Commons_talk:Depicts#Is_there_a_simple_way_to_find_featured/quality/valued_images_tagged_as_depicting_something? is another potential method that may be more reliable

def root_premium_identifier():
    return "premium content"

def main():
    connection = sqlite3.connect('wikimedia_commons_cache.db')
    cursor = connection.cursor()
    create_table(cursor)
    load_premium_image_list_into_database(cursor)
    connection.commit()
    list_premium_images_from_category(cursor, "Category:Lawns")
    connection.commit()

    connection.close()

def create_table(cursor):
    if "cache" in existing_tables(cursor):
        print("cache table exists already, delete file with database to recreate")
    else:
        cursor.execute('''CREATE TABLE cache
                    (filename text, category text, explanation text)''')

def load_premium_image_list_into_database(cursor):
    from_cache = find_all_category_entries_from_cache(cursor, root_premium_identifier())
    if len(from_cache):
        print("some premium entries loaded! Not loading more")
        return
    # give vastly faster loading and skips defeatured images
    # traversing entire https://commons.wikimedia.org/wiki/Category:Highlighted_content
    # requires going through gigantic category structure
    for category in ["Category:Quality images", "Category:Featured_pictures_on_Wikimedia_Commons", "Category:Valued_images"]:
        for entry in mediawiki_api_wrapper.query.pages_from_category(category, URL="https://commons.wikimedia.org/w/api.php"):
            record_file(cursor, entry, root_premium_identifier(), "directly in category")


    # loading from Category:Quality images without recursion is much faster - is similar thing
    # achievable with featured images?
    # https://commons.wikimedia.org/wiki/Category:Quality_images
    # https://commons.wikimedia.org/wiki/Category:Featured_pictures_on_Wikimedia_Commons
    # https://commons.wikimedia.org/wiki/Category:Featured_pictures_on_Wikipedia_by_language (recursion needed)
    # https://commons.wikimedia.org/wiki/Category:Valued_images

    #print(why_in_category(cursor, "nonexisting", category))
    #print(why_in_category(cursor, "File:Gottleuba bikeway Geibeltbad - Mühlenstraße, Pirna121401375.jpg", category))


def list_premium_images_from_category(cursor, category):
    if category.find("Category:"):
        raise Exception("Include Category: prefix")
    load_category_recursive_into_database_from_commons_if_not_loaded(cursor, category)
    from_cache = find_all_category_entries_from_cache(cursor, category)
    found = False
    for entry in from_cache:
        if is_quality_image(cursor, entry[0]):
            print("https://commons.wikimedia.org/wiki/" + entry[0].replace(" ", "_").replace("'", "%27"))
            found = True
    if not found:
        print("Nothing found in", category)

def load_category_recursive_into_database_from_commons_if_not_loaded(cursor, category):
    from_cache = find_all_category_entries_from_cache(cursor, category)
    if len(from_cache) == 0:
        for entry in find_all_category_entries_from_commons(category):
            explanation = " > ".join(entry["path"])
            record_file(cursor, entry["filename"], category, explanation)
        from_cache = find_all_category_entries_from_cache(cursor, category)

def existing_tables(cursor):
    cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
    table_listing = cursor.fetchall()
    #print(table_listing)
    returned = []
    for entry in table_listing:
        returned.append(entry[0])
    return returned

def is_quality_image(cursor, filename):
    return why_in_category(cursor, filename, root_premium_identifier()) != None

def why_in_category(cursor, filename, category):
    cursor.execute("SELECT explanation FROM cache WHERE filename=:filename AND category=:category;", {'filename': filename, 'category': category}) # TODO PARAMETRIZE
    returned = cursor.fetchall()
    if(len(returned) == 0):
        return None
    return returned[0]

def record_file(cur, filename, category, explanation):
    cur.execute("INSERT INTO cache VALUES (:filename, :category, :explanation)", {'filename': filename, 'category': category, 'explanation': explanation})


def find_all_category_entries_from_cache(cursor, category):
    cursor.execute("SELECT filename, explanation FROM cache WHERE category='" + category +  "';") # TODO PARAMETRIZE
    returned = cursor.fetchall()
    return returned

def find_all_category_entries_from_commons(category):
    returned = []
    encountered = []
    categories_for_processing = [{"name": category, "path": []}]
    while len(categories_for_processing) > 0:
        category = categories_for_processing.pop()
        path = category["path"].copy()
        path.append(category["name"])
        print(path)
        for entry in mediawiki_api_wrapper.query.pages_from_category(category["name"], URL="https://commons.wikimedia.org/w/api.php"):
            if entry not in encountered:
                encountered.append(entry)
                if(entry.find("Category:") == 0):
                    if is_overly_broad_spam_category(entry):
                        categories_for_processing.append({"name": entry, "path": path})
                else:
                    returned.append({"filename": entry, "path": path})
    return returned

def is_overly_broad_spam_category(category_name):
    # reduce impact of
    # https://commons.wikimedia.org/w/index.php?title=Commons:Village_pump/Proposals&oldid=685883036#Is_%22Category:Interior_of_Lucas_Oil_Stadium%22_supposed_to_be_an_indirect_subcategory_of_%22Category:Grass%22?
    for banned in ["film)", "(character)", "People of "]:
        if banned in category_name:
            return True
    return False

main()