import mediawiki_api_wrapper
import shared
import mwparserfromhell
import datetime
import password_data
import wiki_category_and_template_structure

print("TODO: check for https://wiki.openstreetmap.org/wiki/Template:User_username on user page")


def skip_based_on_page_text(page_text):
    skipped = [
        # I give up with this category of file, sorry
        "[[Category:Screenshots]]",
        "[[Category:Maps",
        "[[Category:Floor plans",
        "[[Category:Aerial imagery",

        # Very likely to be of valuable variety with no consensus to delete them
        "[[Category:Events",

        # not decided how to handle them
        # see https://wiki.openstreetmap.org/wiki/Talk:Wiki#Designing_policy_for_handling_files_without_clear_license
        "fair use",

        "{{Assumed-CC-BY-SA-2.0-self}}", # should be deleted but there is no consensus to do this
    ] + wiki_category_and_template_structure.screeshot_categories()
    for banned in skipped:
        if banned.lower() in page_text.lower():
            return True

def main():
    split_into_rows_split_grouped_by_priority = []
    for entry in range(100):
        split_into_rows_split_grouped_by_priority.append([])
    for page_title in files_eligible_for_deletion():
        if page_title.find("File:") != 0:
            print(page_title, "is not a file")
            continue
        used_on_wiki = shared.is_file_used_except_on_copyright_warning_pages(page_title)
        page = mediawiki_api_wrapper.query.download_page_text_with_revision_data(page_title)
        page_text = page['page_text']

        if skip_based_on_page_text(page_text):
            continue

        waiting_for_deletion = False
        has_commons_replacement = False
        used_outside_wiki = False
        wikicode = mwparserfromhell.parse(page_text)
        templates = wikicode.filter_templates()
        for template in templates:
            if template.name.strip().lower() == "delete".lower():
                waiting_for_deletion = True
            if template.name.strip().lower() == "Superseded by Commons".lower():
                has_commons_replacement = True
            if template.name.strip().lower() == "Used outside the Wiki".lower():
                used_outside_wiki = True

        problem_priority = 0

        comments = None
        if used_on_wiki or used_outside_wiki:
            if waiting_for_deletion:
                if used_on_wiki and used_outside_wiki:
                    comments = ["marked for deletion, still in use on Wiki and outside it"]
                    problem_priority = 35
                if used_on_wiki and not used_outside_wiki:
                    comments = ["marked for deletion, still in use on Wiki"]
                    problem_priority = 30
                if not used_on_wiki and used_outside_wiki:
                    comments = ["marked for deletion, still in use outside Wiki"]
                    problem_priority = 33
            else:
                if used_on_wiki and used_outside_wiki:
                    comments = ["not marked for deletion, still in use on Wiki and outside it"]
                    problem_priority = 13
                if used_on_wiki and not used_outside_wiki:
                    comments = ["not marked for deletion, still in use on Wiki"]
                    problem_priority = 18
                if not used_on_wiki and used_outside_wiki:
                    comments = ["not marked for deletion, still in use outside Wiki"]
                    problem_priority = 15
        else:
            if not waiting_for_deletion:
                comments = ["not marked for deletion"]
                problem_priority = 20
            else:
                # nothing needs to be done here
                continue

        if comments == None:
            raise
        
        if has_commons_replacement:
            comments.append("replaceable by Commons file")
            problem_priority = 0

        comment = ", ".join(comments)
        if problem_priority >= 30:
            comment = "{{Red|" + comment + "}}"
        elif problem_priority >= 20:
            comment = "{{Orange|" + comment + "}}"
        elif problem_priority >= 10:
            comment = "{{Yellow|" + comment + "}}"
        else:
            print("skipping low priority: " + comment)
            continue
        
        upload_history = mediawiki_api_wrapper.query.file_upload_history(page_title)
        uploader = shared.get_uploader_from_upload_history_or_none_if_not_clear(upload_history, page_title, log_when_returned_none_due_to_multiple_uploaders=True)

        if uploader == None:
            uploader = "multiple"
        else:
            uploader = "[[User:" + uploader + "|" + uploader + "]] [https://www.openstreetmap.org/user/" + uploader.replace(" ", "%20") + " potential OSM user matching wiki account name]"

        split_into_rows_split_grouped_by_priority[problem_priority].append(["[[:" + page_title + "]]" + "|"+comment, uploader])
        print("* [[:" + page_title + "]]")
    
    split_into_rows = []
    for group in split_into_rows_split_grouped_by_priority[::-1]:
        split_into_rows += group
    array = shared.generate_array_wikicode(split_into_rows)
    print(array)

    session = shared.create_login_session()
    shared.ensure_existence_category_for_this_month_notification(session)
    text = get_hint_text() + array + "\n\n{{User:Mateusz Konieczny/todo}}"
    show_page = "User:" + password_data.username() + "/cleanup"
    entry_count = 0
    for group in split_into_rows_split_grouped_by_priority:
        entry_count += len(group)
    edit_summary = "listing files files with expired warnings where action is needed - " + str(entry_count) + " entries"
    mediawiki_api_wrapper.login_and_editing.replace_text_on_page_and_show_diff(session, show_page, text, edit_summary)

def get_hint_text():
    text = ""
    text += "See [[Talk:Wiki#Designing_policy_for_handling_files_without_clear_license|this discussion]] for context what is going on here. See also [[User:Mateusz Konieczny/notify uploaders]] - help with processing this images, especially with finding replacement for already used file is highly welcome.\n<hr>\n"
    text += "Files that were marked as missing copyright info and uploader were notified over six months ago. This files need edit, available options include:\n"
    text += "* removal of spurious warning - maybe original person missed some copyright info on image itself? Maybe someone fixed license info but has not removed warning?\n"
    text += "** [[Wiki:Media file license chart]] may help\n"
    text += "* contacting uploader again, maybe not only via user talkpage and modifying date on template (you can change it to <nowiki>" + shared.file_template_about_missing_license() + "</nowiki> after notifying user again), maybe user has contact info on their Wiki user page?\n"
    text += "* replace uses of file with a better one - files from https://commons.wikimedia.org/ can be directly used, you can also take own one and upload it to Wikimedia Commons\n"
    text += "* remove files without replacement (where image was not useful anyway or on unimportant page like an abandoned proposal), in such case add note on file description page so if licensing situation is fixed such edits can be undone (add text such as <nowiki>Delinked in https://wiki.openstreetmap.org/w/index.php?title=Template:Table:INT-1:M&diff=2365298&oldid=1368610</nowiki>)\n"
    text += "** when delinking consider using non-cryptic edit descriptions such as\n"
    text += "**:file removed for copyright reasons - all Mapillary images are under license requiring attribution of author. If you are the original author of image: you should mention that on upload, file can be recovered or reuploaded with info about uploader (ask on [[Talk:wiki]] if file was deleted already)\n"
    text += "**:file removed for copyright reasons. If you are the original author of image: you should mention that on upload, file can be recovered or reuploaded with info about uploader (ask on [[Talk:wiki]] if file was deleted already)\n"
    text += "* Edit descriptions of files still in use to note proposed replacement images\n"
    text += "* Mark files for deletion by placing for example <nowiki>{{Delete|1=unused, missing license info, uploader was notified over 6 months ago}}</nowiki>\n"
    text += "* Mark files for deletion by placing for example <nowiki>{{Delete|1=delinked, missing license info, uploader was notified over 6 months ago}}</nowiki>\n"
    text += "* [[Contact channels|ask other for help]]\n"
    return text

def files_eligible_for_deletion():
    # currently oldest category is 
    # Category:Media without a license - author notified October 2021

    # check
    # https://wiki.openstreetmap.org/wiki/Category:Media_without_a_license
    # https://wiki.openstreetmap.org/wiki/Category:Media_without_a_proper_attribution
    now = datetime.datetime.now()
    year = now.year
    month = now.month

    max_month_offset = 2021 * 12 + 10 - ( year * 12 + month )

    # when things are eligible for deletion?
    # lets take "uploader notified 2022, May"
    # it can include files from last day of May
    # so one month lapses at last day of 2022-06
    # so two month lapses at last day of 2022-07
    # so six month lapses at last day of 2022-11
    # so on 2022-12-01 one may start deleting photos from 2022-05
    min_month_offset = -7

    return files_where_uploaders_were_notified(range(max_month_offset, min_month_offset + 1))

def files_where_uploaders_were_notified(month_offset_range):
    returned = []
    for offset in month_offset_range:
        print(offset)
        now = datetime.datetime.now()
        year = now.year
        month = now.month + offset
        while month <= 0:
            month += 12
            year -= 1
        checked_date = datetime.date(year, month, 1)
        checked_month_name = checked_date.strftime("%B")
        # generate plausible category names
        # Category:Media without a license - author notified October 2021
        # Category:Media without a license - uploader notified 2022, May
        # Category:Media without a proper attribution - uploader notified 2022, May
        categories = [
            "Category:Media without a license - author notified " + checked_month_name + " " + str(year),
            "Category:Media without a license - uploader notified " + checked_month_name + " " + str(year),
            "Category:Media without a license - uploader notified " + str(year) + ", " + checked_month_name,
            "Category:Media without a proper attribution - uploader notified " + str(year) + ", " + checked_month_name,
        ]
        for category in categories:
            for page_title in mediawiki_api_wrapper.query.pages_from_category(category):
                returned.append(page_title)
        month -= 1
    return returned

main()
