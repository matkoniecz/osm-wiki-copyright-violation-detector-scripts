import mediawiki_api_wrapper
import webbrowser
import shared
import password_data

def false_positives():
    return [
        "not found on https://commons.wikimedia",
        "See https://commons.wikimedia.org/wiki/Commons:Deletion_requests",
        "Derived from [https://commons.wikimedia.org/wiki/File:Sinnbild_Reiter.svg a German road sign]",
        "Derived from [https://commons.wikimedia.org/wiki/File:Zeichen_376.svg a German road sign]",
        "Derived from [https://commons.wikimedia.org/wiki/File:Zeichen_391_-_Mautpflichtige_Strecke,_StVO_2003.svg a German road sign]",
        "Derived from [https://commons.wikimedia.org/wiki/File:Zeichen_131.svg German road sign 131]",
        "Shown is the municipality [http://en.wikipedia.org/wiki/Baarle-Nassau Baarle-Nassau]",
        "View of [http://en.wikipedia.org/wiki/",
        "maybe https://commons.wikimedia.org/wiki/Category:",
        "Shape taken from [https://commons.wikimedia.org/wiki/File:C.1_Die_Fahrwassertiefe_ist_begrenzt.svg here]",
        "Derived from German road signs [https://commons.wikimedia.org/wiki/File:Zeichen_314.svg 314] and [https://commons.wikimedia.org/wiki/File:Zeichen_361-50.svg 361-50]",
        "Derived from [https://commons.wikimedia.org/wiki/File:Zeichen_361-50.svg German road sign 361-50]",

        "https://commons.wikimedia.org/wiki/Commons:Freedom_of_panorama",
        "https://de.wikipedia.org/wiki/Datei:Zeichen_151_-_Unbeschrankter_Bahn%C3%BCbergang,_StVO_1970.svg", # see say https://wiki.openstreetmap.org/wiki/File:State_Steamtrain3.svg
        "derivative work of https://commons.wikimedia.org/",
        "Modified version of https://commons.wikimedia.org",
        "https://en.wikipedia.org/wiki/InterContinental_Warsaw", # https://wiki.openstreetmap.org/wiki/File:Osm2w_intercontinental.jpg
        "https://en.wikipedia.org/wiki/Baarle-Nassau", # https://wiki.openstreetmap.org/wiki/File:Overpass_turbo_multipolygon_test.png
    ]

def main():
    """
    text = get_page_text_from_page_title("File:LA2-slippy-duplicate.png")
    print(get_reason_why_easy_to_recover_license_info(text))
    """
    gathered = []
    all_gathered = []
    for page_title in images():
        #print()
        #print(page_title)
        text = get_page_text_from_page_title(page_title)
        #print(text)
        count = 10
        if get_reason_why_easy_to_recover_license_info(text) != None:
            print()
            print("FOUND")
            link = mediawiki_api_wrapper.interface.osm_wiki_page_link(page_title)
            gathered.append(link)
            all_gathered.append(link)
            print(gathered)
            if len(gathered) >= count:
                for link in gathered:
                    pass
                    #webbrowser.open(link, new=2)
                gathered = []
                #shared.pause()
    text = ""
    text += "Following images are missing licensing info but have description that should allow relatively easy license handling. Help with processing them is highly welcome!\n\n"
    text += "Check linked source - if it was deleted, or is marked as 'all rights reserved' or other restrictive license then this image should be almost certainly no longer used and should be delinked and marked for deletion with {{T|delete}}\n\n"
    text += "If license is fine, then for Wikimedia Commons images mark file with {{T|Superseded by Commons}} (or replace uses and apply {{T|Delete|replaced by Commons image LINK}} - for files with the same name as Commons file just use {{T|Delete|replaced by shadowed Commons image LINK}})\n\n"
    text += "For Flickr images with OK license use https://flickr2commons.toolforge.org/#/ - for transferring Flickr image to Wikimedia Commons, which allows to record license status. Then next steps as with any other Commons image\n\n"
    for link in all_gathered:
        text += "* " + link + "\n"
    session = shared.create_login_session()
    show_page = "User:" + password_data.username() + "/friendly"
    edit_summary = "list files with relatively easy to handle licensing situation - help welcomed with processing - " + str(len(all_gathered)) + " entries"
    text += "\n\n{{User:Mateusz Konieczny/todo}}"
    mediawiki_api_wrapper.login_and_editing.replace_text_on_page_and_show_diff(session, show_page, text, edit_summary)
    if len(all_gathered) == 0:
        raise Exception("stop filtering what is detected by complex_cases")


def get_page_text_from_page_title(page_title):
    page = mediawiki_api_wrapper.query.download_page_text_with_revision_data(page_title)
    return page['page_text']

def get_reason_why_easy_to_recover_license_info(text):
    if "Superseded by Commons".lower() in text.lower():
        return None # soon will not be a problem anymore
    if "{{delete".lower() in text.lower():
        return None # soon will not be a problem anymore
    if "Category:OSM national logos".lower() in text.lower():
        return None # osm logo is a tricky legal mess
    if "rendered in OSM2W".lower() in text.lower():
        return None # false positives and not easy to solve
    if "{{Featured date".lower() in text.lower():
        return None # tricky to handle as featured template should stay anyway
    if "{{dead link}}".lower() in text.lower():
        return None # not actually easy to recover
    if "{{OpenStreetMap trademark}}".lower() in text.lower():
        return None # legal situation of that logo is nightmarish
        
    if "Cropped version of same image on Wikimedia".lower() in text.lower():
        return None
    complex_templates = [
        "part of", # not so bad, but...
        "cropped from", # not so bad, but...
        "cropped version of",
        "crop from",
        "based upon",
        "based on",
        "part of",
        "derived from",
        "derived work from",
        "based on the one from",
    ]
    complex_cases = []
    for entry in complex_templates:
        complex_cases.append(entry + " http://commons.wikimedia.org")
        complex_cases.append(entry + " https://commons.wikimedia.org")
        complex_cases.append(entry + " [http://commons.wikimedia.org")
        complex_cases.append(entry + " [https://commons.wikimedia.org")
    for entry in false_positives() + complex_cases:
        text = text.replace(entry, "")
    indicators_of_easy_license_recovery = ["commons.wikimedia.org", "flickr.com", "wikipedia.org"]
    for checked in indicators_of_easy_license_recovery:
        if checked.lower() in text.lower():
            return checked
    return None

def images():
    for file in mediawiki_api_wrapper.query.uncategorized_images():
        yield file
    for file in mediawiki_api_wrapper.query.pages_from_category("Category:Media without a license"):
        yield file
    for file in mediawiki_api_wrapper.query.pages_from_category("Category:Attribution not provided in CC license template which requires attribution"):
        yield file

main()