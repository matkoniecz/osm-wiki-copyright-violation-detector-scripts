import mediawiki_api_wrapper
import shared
import mwparserfromhell
import datetime

def get_page_text_from_page_title(page_title):
    page = mediawiki_api_wrapper.query.download_page_text_with_revision_data(page_title)
    return page['page_text']

def main():
    print("{{CC-BY-SA-2.0 OpenStreetMap}} {{OSM Carto screenshot||old_license}} {{JOSM screenshot with imagery|{{Imagery license}}|old_license}}")
    print("https://wiki.openstreetmap.org/wiki/Template_talk:JOSM_screenshot_with_imagery/doc may be useful to solve first")
    for page_title in mediawiki_api_wrapper.query.pages_from_category("Category:ODbL OpenStreetMap"):
        upload_history = mediawiki_api_wrapper.query.file_upload_history(page_title)

        upload_time = mediawiki_api_wrapper.query.get_the_first_upload_date_from_file_history(upload_history)
        if upload_time > datetime.datetime(2012, 9, 13):
            continue
        upload_time = mediawiki_api_wrapper.query.get_upload_date_from_file_history(upload_history)
        if upload_time != None:
            if upload_time < datetime.datetime(2012, 9, 13):
                print()
                print("case where ODbL OpenStreetMap is used too early")
                print(upload_time)
                print(datetime.datetime(upload_time.year, upload_time.month, upload_time.day) < datetime.datetime(2012, 9, 13))
                print("https://wiki.openstreetmap.org/wiki/"+page_title.replace(" ", "_"))
                print(get_page_text_from_page_title(page_title))
                print()
            else:
                raise "absolutely impossible, WTF"
        else:
            print("https://wiki.openstreetmap.org/wiki/"+page_title.replace(" ", "_"))

main()
