import mediawiki_api_wrapper
import shared
import datetime

def selftest():
    pass

def main():
    selftest()
    session = shared.create_login_session()

    for page_title in mediawiki_api_wrapper.query.all_pages():
        print(page_title)
        mediawiki_api_wrapper.login_and_editing.null_edit(session, page_title)

    for page_title in mediawiki_api_wrapper.query.pages_from_category("Category:Disabled"):
        print(page_title)
        mediawiki_api_wrapper.login_and_editing.null_edit(session, page_title)

main()