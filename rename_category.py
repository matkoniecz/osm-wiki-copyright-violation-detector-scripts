import mediawiki_api_wrapper
import shared
import mwparserfromhell
import datetime
import password_data

def main():
    old_cat = "Category:User images"
    session = shared.create_login_session('image_bot')
    for page_title in mediawiki_api_wrapper.query.pages_from_category(old_cat):
        if page_title.find("User:" + password_data.username() + "/notify uploaders/") == 0:
            page_text = ""
            # TODO get text
            mediawiki_api_wrapper.login_and_editing.replace_text_on_page_and_show_diff(session, page_title, page_text, edit_summary, mark_as_bot_edit=True)

main()
