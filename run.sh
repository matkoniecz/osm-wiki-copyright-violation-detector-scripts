python3 find_unused_bing_imagery.py

# cleanup after myself
python3 edit_blank_file_listing_pages_polluting_maintenance_categories.py
# requires no manual action, may trigger others into helping
python3 list_files_where_license_warnings_expired.py
python3 list_pages_with_easy_to_recover_licenses.py
python3 list_time_travelling_odbl.py
python3 edit_replace_file.py # going through all entries is feasible
python3 edit_add_clear_attribution_for_self_templates.py # going through all entries is feasible 
python3 edit_note_files_used_in_osm_database.py # going through all entries is feasible

# select random one
# https://unix.stackexchange.com/questions/81566/run-commands-at-random
if (( RANDOM % 2 )); then 
python3 complain_about_missing_file_source_or_license.py;
else 
python3 complain_about_missing_attribution_where_license_requires_it.py;
fi

python3 edit_remove_double_categorization_of_JOSM_imagery.py
echo "list_time_travelling_odbl should generate OSM Wiki page TODO"
echo "https://wiki.openstreetmap.org/wiki/Category:Files_composed_of_bing_aerial_imagery
apply on unused ones {{delete|Unused Bing image and we have no permission to keep them - unused do not qualify even for fair use}}"
echo "start transferring unused files with PD templates and other from OSM Wiki to Commons, see https://wiki.openstreetmap.org/wiki/File:NO_306_9.svg and other from https://wiki.openstreetmap.org/wiki/Category:Norway"