import mediawiki_api_wrapper
import urllib.parse
import password_data
import time
import random
import datetime

def ensure_existence_category_for_this_month_notification(session):
    category_name = entire_category_name()
    category_text = category_for_given_month_page_text()
    data = mediawiki_api_wrapper.query.download_page_text_with_revision_data(category_name)
    if data == None:
        mediawiki_api_wrapper.login_and_editing.create_page_and_show_diff(session, category_name, category_text, "create category for holding files awaiting response")
    elif data['page_text'] != category_text:
        mediawiki_api_wrapper.login_and_editing.edit_page_and_show_diff(session, category_name, category_text, "reset category for holding files awaiting response", data['rev_id'], data['timestamp'])


def category_for_given_month_page_text():
    return """Use <pre>""" + file_template_about_missing_license() + """</pre>to put file here.

Please, use solely only where someone who uploaded file was notified during this month.

[[Category:Media without a license]]"""

def file_template_about_missing_license():
    return "{{Unknown|subcategory=" + subcategory_selectable_name_part() + "}}"

def subcategory_selectable_name_part():
    mydate = datetime.datetime.now()
    current_month = mydate.strftime("%B")
    current_year = mydate.strftime("%Y")
    return "uploader notified " + current_year + ", " + current_month

def entire_category_name():
    return "Category:Media without a license - " + subcategory_selectable_name_part()

def description_keywords_that_may_indicate_that_author_is_specified():
    return ["selbst", "own work", "taken by me", "self made", "self-made", "by author",
    "I made", "I took this photo", "I took", "own photo",
    "image by", "picture by", "taken by", "photograph by", "photo by", "photograph taken",
    "Photo by", "image by", "copied from", "my own original work", "my own",
    "Foto eigenes Werk", # "Photo own work"
    "eigenes Foto", "eigenes",
    "Based on OSM data", "Based on", "OSM data", "source", "OSM contributors", "Openstreetmap contributors",
    "Derived from",
    "commons", "wikipedia", "0px-", "px-", "author", "flickr"]

def users_dropped_from_regular_processing():
    return [
    "Geri-oc", # All my (geri-oc) pictures taken by myself for documentation in OpenStreetMap and {{CC0-self}} and can be corrected accordingly. Please decide for yourself in the future, because at the age of 73 I am no longer so resilient in terms of health. https://wiki.openstreetmap.org/w/index.php?title=User_talk:Geri-oc&curid=86920&diff=2296959&oldid=2296556 
    "Hoream telenav", # received PM from them
    "!i!", "Bwurst", # many PD-shape/Carto/possibly PD-shape images
    "Marek kleciak", # lost computer access
    "Marek Kleciak", # ??? probably the same person as above
    "VIPINDAS K", # problematic: copyvios related to OSM
    "Dmgroom", # handle https://wiki.openstreetmap.org/wiki/File:Baghdad-210207.png first
    "GeoJ", # https://wiki.openstreetmap.org/wiki/User_talk:GeoJ
    "Ulfm", "Abunai", # murdered - https://wiki.openstreetmap.org/w/index.php?title=User_talk%3AMateusz_Konieczny&type=revision&diff=2282967&oldid=2282571 See also https://wiki.openstreetmap.org/wiki/User:Mateusz_Konieczny/notify_uploaders/Abunai
    "Malenki", # dead - https://wiki.openstreetmap.org/wiki/User_talk:Malenki
    "Bmwiedemann", # tricky https://wiki.openstreetmap.org/wiki/File:Luftbild-2-originalprojektion-unscaled.png asked in https://wiki.openstreetmap.org/w/index.php?title=User_talk:Bmwiedemann&diff=2370529&oldid=104624
    "Reneman", # most of files are not actually theirs - see descriptions of files, see also https://wiki.openstreetmap.org/wiki/User:Mateusz_Konieczny/notify_uploaders/Reneman

    "Tallguy", # bunch of tricky screenshots, see https://wiki.openstreetmap.org/wiki/User_talk:Tallguy
    "Jharvey", # Screenshot mess
    "Amᵃᵖanda", # Tricky issues with OSMF logo - and they got already message about it
    "Amunizp", # lets skip it for now, too confusing
    "Nillerdk", # unclear attribution situation, skip for now
    "Negreheb", # handle https://wiki.openstreetmap.org/wiki/File:Example_guard_rail_direction.jpg first
    "Zermes", # https://wiki.openstreetmap.org/wiki/File:Barrier_side.png
    "Nyk0la3", # https://wiki.openstreetmap.org/wiki/User_talk:Nyk0la3
    "Kumakyoo", # See https://wiki.openstreetmap.org/wiki/User:Mateusz_Konieczny/notify_uploaders/Kumakyoo and https://wiki.openstreetmap.org/w/index.php?title=User_talk:Kumakyoo&diff=2370199&oldid=2364172
    "Severin.menard", # https://wiki.openstreetmap.org/wiki/File:OSM_account_creation_contributors_terms_webpage_FR.png
    "Nyampire", # https://wiki.openstreetmap.org/wiki/File:Fukaya_ortho_imagery.png handle first
    "Danidin9", # notified in https://wiki.openstreetmap.org/wiki/User_talk:Danidin9#Aerial_imagery_source - adding templates on file pages can be done but is a bit waste of time
    "Kempelen", # skip due to https://wiki.openstreetmap.org/wiki/File:Mary_purple2_hu.png depending on unsolved one
    "Westnordost", # first process StreetComplete
    "SimonPoole", # reask only manually
    ]

def create_login_session(index = 'api_password'):
    login_data = password_data.api_login_data(index)
    password = login_data['password']
    username = login_data['user']
    session = mediawiki_api_wrapper.login_and_editing.login_and_create_session(username, password)
    return session

def is_file_used_except_on_copyright_warning_pages(page_title, verbose=True):
    for page in mediawiki_api_wrapper.query.pages_where_file_is_used_as_image(page_title):
        if verbose:
            print(page_title, "used on", page)
        if is_copyright_warning_listing(page):
            continue
        return True
    return False

def is_copyright_warning_listing(page_title):
    if "/notify uploaders/" in page_title:
        return True
    return False

def make_test_edit(session):
    # session = shared.create_login_session()
    sandbox = "Sandbox"
    data = mediawiki_api_wrapper.query.download_page_text_with_revision_data(sandbox)
    mediawiki_api_wrapper.login_and_editing.edit_page_and_show_diff(session, sandbox, data['page_text'] + "\ntetteteteetteetett", "edit summary", data['rev_id'], data['timestamp'])

def generate_table_showing_image_data_for_review(data, break_after=None):
    """
    data - iterable with dictionaries containing following fields:
    page_title
    page_text
    """
    generated_summary_parts = []
    for entry in data:
        page_title = entry['page_title']
        page_text = entry['page_text']
        upload_timestamp = "unknown upload time"
        if 'upload_time' in entry:
            upload_timestamp = str(entry['upload_time'])
        generated_summary_parts.append("[[" + page_title + "|thumb| ["+ mediawiki_api_wrapper.interface.osm_wiki_page_edit_link(page_title) + " (EEEEEEEEEEEEEEEEEEEEEEEDIIIIIIIIIIIIIIIIIIIIIIIIIIIIIT EEEEEEEEEEEEEEEEEEEEEEEDIIIIIIIIIIIIIIIIIIIIIIIIIIIIIT EEEEEEEEEEEEEEEEEEEEEEEDIIIIIIIIIIIIIIIIIIIIIIIIIIIIIT EEEEEEEEEEEEEEEEEEEEEEEDIIIIIIIIIIIIIIIIIIIIIIIIIIIIIT EEEEEEEEEEEEEEEEEEEEEEEDIIIIIIIIIIIIIIIIIIIIIIIIIIIIIT EEEEEEEEEEEEEEEEEEEEEEEDIIIIIIIIIIIIIIIIIIIIIIIIIIIIIT EEEEEEEEEEEEEEEEEEEEEEEDIIIIIIIIIIIIIIIIIIIIIIIIIIIIIT)] [[:" + page_title + "]] text on page: <<nowiki>" + page_text + "</nowiki>> upload timestamp: " + upload_timestamp + "]]\n")
        if break_after != None:
            if len(generated_summary_parts) == break_after:
                generated_summary_parts.append("BREAK requested after " + str(break_after) + " images\n")
    return generate_matrix_array(generated_summary_parts, break_after)

def generate_matrix_array(displayed_parts, break_after):
    columns = 3
    split_into_rows = []
    new_row = []
    index = 0

    while index < len(displayed_parts):
        if (index % columns) == 0:
            if len(new_row) > 0:
                split_into_rows.append(new_row)
            new_row = []
        new_row.append(displayed_parts[index])
        index += 1
    if new_row != []:
        split_into_rows.append(new_row)
    return generate_array_wikicode(split_into_rows)

def generate_array_wikicode(split_into_rows):
    # split_into_rows - list of lists (list of row, each represented by a list)

    output = '{| class="wikitable"\n'
    for row in split_into_rows:
        output += "|-\n"
        output += "| " + " || ".join(row) + "\n"
    output += "|}\n"
    return output

def pause():
    # wait
    print("press enter (any key?) to continue")
    input()

def ask_human():
    while True:
        print("press enter y to continue, n to reject")
        got = input()
        if got == "y":
            return True
        if got == "n":
            return False

def make_delay_after_edit():
    sleep_time = 0
    if random.randrange(1, 100) > 80:
        sleep_time += random.randrange(0, 5) 
    if random.randrange(1, 100) > 95:
        sleep_time += random.randrange(800, 1200) 
    print("make_delay_after_edit (", sleep_time ,") <-start")
    time.sleep(sleep_time)
    print("make_delay_after_edit end->")

def get_uploader_of_file_or_none_if_not_clear(page_title, log_when_returned_none_due_to_multiple_uploaders=True):
    upload_history = mediawiki_api_wrapper.query.file_upload_history(page_title)
    return get_uploader_from_upload_history_or_none_if_not_clear(upload_history, page_title, log_when_returned_none_due_to_multiple_uploaders)

def get_uploader_from_upload_history_or_none_if_not_clear(upload_history, page_title, log_when_returned_none_due_to_multiple_uploaders):
    if upload_history == None:
        return None # TODO: remove root cause of THAT
    uploader = mediawiki_api_wrapper.query.get_uploader_from_file_history(upload_history)
    if uploader == None and log_when_returned_none_due_to_multiple_uploaders:
        print("Unable to establish uploader")
        print("https://wiki.openstreetmap.org/wiki/"+page_title.replace(" ", "_"))
        return None
    return uploader

