import mwparserfromhell
import shared
import mediawiki_api_wrapper

session = shared.create_login_session('image_bot')
for page_title in mediawiki_api_wrapper.query.pages_from_category("Category:Media without a proper attribution"):
    data = mediawiki_api_wrapper.query.download_page_text_with_revision_data(page_title)
    edit_summary = "fix capitalization in template hint"
    text = data["page_text"]
    code = mwparserfromhell.parse(text)
    for template in code.filter_templates():
        if template.name.matches("Proper attribution missing"):
            print(template)
            for param in template.params:
                key = param.split("=")[0].strip()
                value = param.split("=")[1].strip()
                if(key == "template"):
                    if value.upper() != value:
                        template.add(key, value.upper()) # TODO report as missing docs? I needed to dig into https://github.com/earwig/mwparserfromhell/blob/7738752b016b4c8f4dd3b381ca125724ac2a3af7/src/mwparserfromhell/nodes/template.py#L247
                        mediawiki_api_wrapper.login_and_editing.edit_page_and_show_diff(session, page_title, str(code), edit_summary, data['rev_id'], data['timestamp'])
