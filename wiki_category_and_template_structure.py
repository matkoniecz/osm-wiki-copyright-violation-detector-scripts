import mediawiki_api_wrapper
import mwparserfromhell
import mediawiki_api_wrapper
import shared

def is_marked_with_template_declaring_licensing_status(page_text):
    return is_any_of_this_templates_present_in_page_source_code(page_text, valid_licencing_template_names())

def templates_marking_as_without_source():
    return ["Unknown", "No licence"]

def primary_no_license_template():
    return templates_marking_as_without_source()[0]

def has_license_marking_as_without_source(page_text):
    return is_any_of_this_templates_present_in_page_source_code(page_text, templates_marking_as_without_source())

def screeshot_categories():
    return [
        "Category:Screenshots",
        "Category:Images of Osmand",
        "Category:GPSMapEdit screenshots",
        "Category:GPSMapEdit screenshots",
        "Category:Images of ID",
        "Category:Images of Osmand",
        "Category:Images of OSMTracker (Android)",
        "Category:Images of Vespucci",
        "Category:Ito OSM Mapper screenshots",
        "Category:Images of Java Applet",
        "Category:Mapillary screenshots",
        "Category:MapSource screenshots",
        "Category:Images of Potlatch2",
        "Category:Images of OSM Garmin maps",
        #"Category:Images of JOSM", # Many are {{JOSM screenshot without imagery}} - TODO: recheck
        "Category:Screenshots of notification mails",
    ]

def map_categories():
    return [
        "Category:Maps of places by continent",
        "Category:Maps of places in Africa",
        "Category:Maps of places in Antarctica",
        "Category:Maps of places in Asia",
        "Category:Maps of places in Europe",
        "Category:Maps of places in North America",
        "Category:Maps of places in Oceania",
        "Category:Maps of places in South America",
        "Category:Maps of places in the United States",
        "Category:Maps of places in Austria",
        "Category:Maps of places in Belgium",
        "Category:Maps of places in Bulgaria",
        "Category:Maps of places in Denmark",
        "Category:Maps of places in Finland",
        "Category:Maps of places in France",
        "Category:Maps of places in Germany",
        "Category:Maps of places in Iceland",
        "Category:Maps of places in Italy",
        "Category:Maps of places in the Netherlands",
        "Category:Maps of places in Norway",
        "Category:Maps of places in Poland",
        "Category:Maps of places in Portugal",
        "Category:Maps of places in Sweden",
        "Category:Maps of places in Switzerland",
        "Category:Maps of places in Turkey",
        "Category:Maps of places in the United Kingdom",
        "Category:Maps of places in Baden-Württemberg",
        "Category:Maps of places in Bayern",
        "Category:Maps of places in Berlin",
        "Category:Maps of places in Nordrhein-Westfalen",
        "Category:Maps of places in Sachsen",
        "Category:Maps of places in Sachsen-Anhalt",
        "Category:Maps of Turku",
        "Category:Map exports of Turku",
        "Category:Map sources of Turku",
        "Category:Maps of Brussels Capital Region",
        "Category:Maps of Flanders",
        "Category:Maps of Wallonia",
        "Category:Maps of places in Brussels Capital Region",
        "Category:Maps of places in Flanders",
        "Category:Maps of places in Wallonia",
        "Category:Maps of Brabant wallon",
        "Category:Maps of Hainaut",
        "Category:Maps of Province de Liège",
        "Category:Maps of Province de Luxembourg",
        "Category:Maps of Province de Namur",
        "Category:Maps of places in Brabant wallon",
        "Category:Maps of places in Hainaut",
        "Category:Maps of places in Province de Liège",
        "Category:Maps of places in Province de Luxembourg",
        "Category:Maps of places in Province de Namur",
        "Category:Maps of Provincie Antwerpen",
        "Category:Maps of Limburg (België)",
        "Category:Maps of Oost-Vlaanderen",
        "Category:Maps of Vlaams-Brabant",
        "Category:Maps of West-Vlaanderen",
        "Category:Maps of places in Provincie Antwerpen",
        "Category:Maps of places in Limburg (België)",
        "Category:Maps of places in Oost-Vlaanderen",
        "Category:Maps of places in Vlaams-Brabant",
        "Category:Maps of places in West-Vlaanderen",
        "Category:Maps of places in Lebanon",
        "Category:Maps of places in Pakistan",
    ]

def all_subcategories(session, root_category, found=None):
    found = [root_category]
    remaining = [root_category]
    while len(remaining) > 0:
        category = remaining.pop()
        for page_title in mediawiki_api_wrapper.query.pages_from_category(category):
            if "Category:" in page_title:
                if page_title not in found:
                    found.append(page_title)
                    remaining.append(page_title)
    return found

def valid_licencing_template_names():
    return [
        # not strictly licenses, but as long as these are present license processing is not needed
        "delete", # active deletion request waiting for processing means that page is processed for now
        "Superseded by Commons", # special deletion variant

        "PD", # in far future it may be worth replacing
        "PD-self",
        "PD-creator",
        "PD-shape",
        "PD-text",
        "PD-textlogo",
        "PD-Turkmen",

        "CC0",
        "OSM Carto icon", # CC0 with extra info about source
        "OpenCampingMapIcons", # CC0 with extra info about source
        "CC0-self",
        "CC-SA-1.0",
        "CC-BY-2.0",
        "CC-BY-2.0-self",
        "CC-BY-2.5",
        "CC-BY-2.5-self",
        "CC-BY-3.0",
        "CC-BY-3.0-self",
        "CC-BY-4.0",
        "CC-BY-4.0-self",
        "CC-BY-SA-2.0",
        "CC-BY-SA-2.0-self",
        "CC-BY-SA-2.5",
        "CC-BY-SA-2.5-self",
        "CC-BY-SA-3.0",
        "CC-BY-SA-3.0-self",
        "CC-BY-SA-4.0",
        "CC-BY-SA-4.0-self",
        "CC-BY-SA-2.0 OpenStreetMap",
        "Geograph",
        "GFDL",
        "GPL",
        "ISC",
        "ID screenshot", # typically formatted "iD screenshot" - TODO: promote two latter ones
        "ID screenshot with imagery",
        "ID screenshot without imagery",
        "Bing image",
        "JOSM Icon license",
        "JOSM screenshot without imagery",
        "JOSM screenshot with imagery",
        "ODbL OpenStreetMap",
        "OSM Carto screenshot",
        "OSM Humanitarian screenshot",
        "Tracesmap screenshot",
        "OSM Tracestrack Topo screenshot",
        "ODbL",
        "Tiles@Home screenshot",
        "PD-PRC-Road Traffic Signs",
        "WTFPL",
        "WTFPL-self",
        "Licence Ouverte",
        "Licence Ouverte 2",
        "Mapbox image",
        "Maxar image",
        "Maxar image portions", # TODO - not sufficient by itself
        "Esri image",
        "Apache",
        "Apache license 2.0",
        "AGPL",
        "LGPL",
        "MIT",
        "MPL",
        "BSD",
        "Open Government Licence 2.0 Canada",
        "PD-B-road-sign", # try to get rid of it by migration
        "PD-CAGov",
        "PD-old",
        "PD-RU-exempt",
        "PD-USGov",
        "PD-USGov-NOAA",
        "OpenSeaMap symbol", # TODO: recheck its state https://wiki.openstreetmap.org/w/index.php?title=Template:OpenSeaMap_symbol&action=history

        # This can be a crayon license :(
        # TODO: investigate!
        "Attribution",

        # This templates should be eliminated (TODO HACK)
        "CC-BY-NC-ND-2.0",
        "CC-BY-NC-ND-4.0",
        "CC-BY-NC-SA-3.0",
        "CC-BY-NC-SA-2.0",
        "CC-BY-ND-2.0",
        "CC-BY-NC", # https://wiki.openstreetmap.org/wiki/Template_talk:CC-BY-NC

        # likely problematic, but at less well defined state
        "Bing image",

        "Mapof", # misleading nowadays: TODO: eliminate it from wiki or retitle or something
        "OSM Carto example", # badly named one... TODO: rename

        # likely problematic but there is no support to delete such images
        "Assumed-CC-BY-SA-2.0-self",

        # https://wiki.openstreetmap.org/wiki/Category:Media_license_templates
    ]

def license_selfcheck():
    missing_licences = False
    category_with_license_templates = "Category:Media license templates"
    licenses_on_wiki_list = list(mediawiki_api_wrapper.query.pages_from_category(category_with_license_templates))

    for page_title in licenses_on_wiki_list:
        name = page_title.replace("Template:", "")
        if name in ["Self-made-image", "Panoramafreiheit", "Personality rights",
                    "OpenStreetMap trademark", "Trademarked", "Free media",
                    "Free screenshot"]:
            # building blocks, not actual license
            continue
        if name in ["Bing image portions"]:
            # insufficient by itself
            continue
        if name in templates_marking_as_without_source():
            # warns about lack of license
            continue
        if name in ["Wiki:Media file license chart"]:
            # likely should not be there, as well...
            continue
        if "File:" in name:
            print(name)
            print("FILE in category with licensing TEMPLATES - something went wrong")
            continue
        if name not in valid_licencing_template_names():
            print(name)
            missing_licences = True
    if missing_licences:
        raise Exception("there are entries in " + category_with_license_templates + " but not listed as licences in this script in valid_licencing_template_names()")
    for licence in valid_licencing_template_names():
        if licence in ["delete", "Superseded by Commons"]:
            # warns about terminal lack of license
            continue
        if licence in ["PD-text", "PD-textlogo", "Apache"]:
            # redirect
            continue
        template_page = "Template:" + licence
        template_doc_page = template_page + "/doc"
        if template_page not in licenses_on_wiki_list:
            session = shared.create_login_session()
            # TODO - can we trigger null edit without session? action=purge?
            mediawiki_api_wrapper.login_and_editing.null_edit(session, template_doc_page)
            mediawiki_api_wrapper.login_and_editing.null_edit(session, template_page)
            licenses_on_wiki_list = list(mediawiki_api_wrapper.query.pages_from_category(category_with_license_templates))
            if template_page not in licenses_on_wiki_list:
                raise Exception(licence + " is missing on wiki in its category (usually making null edits on template and /doc subpage fixes problem, but it was tried already here)")

def is_any_of_this_templates_present_in_page_source_code(page_text, license_name_list):
    wikicode = mwparserfromhell.parse(page_text)
    templates = wikicode.filter_templates()
    for template in templates:
        for valid in license_name_list:
            if template.name.strip().lower() == valid.lower():
                return True
    return False

